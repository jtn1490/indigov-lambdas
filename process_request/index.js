const axios = require("axios");

// Configure sendgrid
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(process.env.SENDGRID);
const emailFrom = process.env.EMAIL_FROM;
const joke_url = process.env.JOKE_URL;

const sendEmail = async opts => {
  const results = await sgMail.send(opts);
  return results;
};

exports.handler = async event => {
  const { data: jokeResponse } = await axios.get(joke_url, {
    headers: {
      Accept: "application/json"
    }
  });
  const { joke } = jokeResponse;

  const promises = event.Records.map(async record => {
    const email = record.messageAttributes.Email.stringValue;
    const emailOptions = {
      to: email,
      from: emailFrom,
      subject: "Open up for a dad joke!",
      html: `<strong>${joke}</strong>`
    };

    await sendEmail(emailOptions);
  });

  return Promise.all(promises);
};
