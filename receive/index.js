// Configure sendgrid
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(process.env.SENDGRID);

// Configure AWS
const AWS = require("aws-sdk");
AWS.config.update({ region: "us-east-1" });
const sqs = new AWS.SQS({ apiVersion: "2012-11-05" });
const queueURL = process.env.SQS_URL;
const EMAIL_FROM = process.env.FROM;

exports.handler = async event => {
  const { email } = event;
  const opts = {
    to: email,
    from: EMAIL_FROM,
    subject: "Thanks!",
    html:
      "<strong>Thanks for your email. Check your inbox soon for a joke!</strong>"
  };

  const emailResponse = await sendEmail(opts);
  const sqsResponse = await sendSqsMessage(email);
  const emailStatusCode = emailResponse[0].statusCode;

  return {
    emailStatusCode,
    sqsResponse
  };
};

const sendEmail = async opts => {
  const results = await sgMail.send(opts);
  return results;
};

const sendSqsMessage = async email => {
  const messageParams = {
    DelaySeconds: 10,
    MessageAttributes: {
      Email: {
        DataType: "String",
        StringValue: email
      }
    },
    MessageBody: "To be, or not to be???",
    QueueUrl: queueURL
  };

  const sqsResponse = await sqs
    .sendMessage(messageParams)
    .promise()
    .then(data => {
      return data;
    })
    .catch(error => {
      console.error("error", error);
      throw error;
    });

  return sqsResponse;
};
