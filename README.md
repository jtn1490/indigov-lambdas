## Lambda functions

This repo consists of two lambda functions: `receive` and `process_request`. The `receive` function expects an `email`. It emails the user to confirm that their email has been received and that they should expect a joke in their inbox soon. The `receive` function then adds a job to the SQS queue. Once the message has been added to the queue, the SQS queue triggers a call to the `process_request` function with retrieves a dad joke from an external API and emails the joke to the user.

The `receive` lambda is exposed by the `https://mn0l5w2eti.execute-api.us-east-1.amazonaws.com/first` endpoint. The `process_request` lambda is not exposed via an API.
